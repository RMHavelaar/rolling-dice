/** @format */

let num = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
let count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

function rollDice() {
  let d1 = Math.ceil(Math.random() * 6);
  let d2 = Math.ceil(Math.random() * 6);
  let total = d1 + d2;
  return total;
}
for (i = 1; i <= 1000; i = i + 1) {
  let result = rollDice();
  count[result] = count[result] + 1;
}

let destination = document.getElementById("graph_box");
let widthMultipler = 2;

for (let j = 2; j < count.length; j = j + 1) {
  let row = document.createElement("div");
  row.style.display = "flex";

  let col1 = document.createElement("div");
  col1.style.width = "50px";
  col1.style.backgroundColor = "#d9bf77";
  col1.style.border = "5px solid black";
  col1.style.TextAlign = "center";
  col1.style.borderRadius = "5px";
  col1.style.color = "black";
  col1.style.fontWeight = "900";
  col1.style.textAlign = "center";
  let col1Text = document.createTextNode(num[j - 2]);
  col1.appendChild(col1Text);
  row.appendChild(col1);

  let col2 = document.createElement("div");
  col2.style.backgroundColor = "#d8ebb5";
  col2.style.border = "5px solid black";
  col2.style.paddingBottom = "5px";
  col2.style.color = "black";
  col2.style.col2TextAlign = "left";
  col2.style.borderRadius = "5px";
  col2.style.width = count[j] * widthMultipler + "px";
  let col2Text = document.createTextNode(count[j]);
  col2.appendChild(col2Text);
  row.appendChild(col2);
  destination.appendChild(row);
}

// I used lessons from A smarter way to JS to get down the DOM actions and the Math functions.  Other than that I was able to come up with this and am super proud of it.
